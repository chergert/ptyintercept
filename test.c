/* test.c
 *
 * Copyright © 2018 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gtk/gtk.h>
#include <vte/vte.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>

#include "ptyintercept.h"

static pty_intercept_t *real_intercept;
static GPtrArray *regexes;

static void
child_setup_cb (gpointer user_data)
{
  setsid ();
  setpgid (0, 0);

  if (ioctl (STDOUT_FILENO, TIOCSCTTY, 0) == -1)
    g_warning ("Failed to setup TTY");
}

static void
wait_cb (GObject      *object,
         GAsyncResult *result,
         gpointer      user_data)
{
  g_subprocess_wait_finish (G_SUBPROCESS (object), result, NULL);
  gtk_main_quit ();
}

static void
resize_window_cb (VteTerminal   *terminal,
                  GtkAllocation *alloc,
                  VtePty        *pty)
{
  gint rows = 0, columns = 0;

  vte_pty_get_size (pty, &rows, &columns, NULL);
  pty_intercept_set_size (real_intercept, rows, columns);

#if 0
  g_print ("Resize window: rows=%u columns=%u\n", rows, columns);
#endif
}

static guint8 *
remove_color_escapes (const guint8 *src,
                      gsize         len,
                      gsize        *out_len)
{
  g_autoptr(GByteArray) dst = g_byte_array_sized_new (len);

  for (gsize i = 0; i < len; i++)
    {
      guint8 ch = src[i];
      guint8 next = (i+1) < len ? src[i+1] : 0;

      if (ch == '\\' && next == 'e')
        {
          i += 2;
        }
      else if (ch == '\033')
        {
          i++;
        }
      else
        {
          g_byte_array_append (dst, &ch, 1);
          continue;
        }

      if (i >= len)
        break;

      if (src[i] == '[')
        i++;

      if (i >= len)
        break;

      for (; i < len; i++)
        {
          ch = src[i];

          if (g_ascii_isdigit (ch) || ch == ' ' || ch == ';')
            continue;

          break;
        }
    }

  *out_len = dst->len;

  return g_byte_array_free (g_steal_pointer (&dst), FALSE);
}

static void
on_data_recv_cb (const pty_intercept_t      *intercept,
                 const pty_intercept_side_t *side,
                 const guint8               *data,
                 gsize                       len,
                 gpointer                    user_data)
{
  g_autofree guint8 *cleaned = NULL;
  gsize cleaned_len = 0;

  g_assert (intercept != NULL);
  g_assert (side != NULL);
  g_assert (side == &intercept->master);
  g_assert (data != NULL);
  g_assert (len > 0);

  cleaned = remove_color_escapes (data, len, &cleaned_len);

  if (cleaned)
    {
      data = cleaned;
      len = cleaned_len;
    }

  for (guint i = 0; i < regexes->len; i++)
    {
      GRegex *regex = g_ptr_array_index (regexes, i);
      g_autoptr(GMatchInfo) match_info = NULL;

      if (g_regex_match_full (regex, (gchar *)data, len, 0, 0, &match_info, NULL))
        {
          g_auto(GStrv) parts = g_match_info_fetch_all (match_info);
          g_autofree gchar *line = g_strjoinv (" ", parts);

          g_print ("%s\n", line);
        }
    }
}

#define ERROR_REGEX1 \
  "(?<filename>[a-zA-Z0-9\\-\\.\\/_]+):" \
  "(?<line>\\d+):"                       \
  "(?<column>\\d+): "                    \
  "(?<level>[\\w\\s]+): "                \
  "(?<message>.*)"

gint
main (gint argc,
      gchar *argv[])
{
  g_auto(GStrv) sub_env = g_get_environ ();
  g_autoptr(GSubprocessLauncher) launcher = NULL;
  g_autoptr(GSubprocess) subprocess = NULL;
  g_autoptr(GError) error = NULL;
  g_auto(pty_fd_t) slave_fd = PTY_FD_INVALID;
  GtkWindow *window;
  VteTerminal *terminal;
  VtePty *pty;
  pty_intercept_t intercept;
  pty_fd_t master_fd;
  gboolean r;

  gtk_init (&argc, &argv);

  regexes = g_ptr_array_new_with_free_func ((GDestroyNotify)g_regex_unref);
  g_ptr_array_add (regexes, g_regex_new (ERROR_REGEX1, G_REGEX_CASELESS, 0, &error));

  window = g_object_new (GTK_TYPE_WINDOW,
                         "title", "PTY Test",
                         "default-width", 800,
                         "default-height", 600,
                         "visible", TRUE,
                         NULL);

  terminal = g_object_new (VTE_TYPE_TERMINAL,
                           "visible", TRUE,
                           NULL);
  gtk_container_add (GTK_CONTAINER (window), GTK_WIDGET (terminal));

  pty = vte_pty_new_sync (VTE_PTY_DEFAULT, NULL, NULL);
  vte_terminal_set_pty (terminal, pty);

  master_fd = vte_pty_get_fd (pty);
  r = pty_intercept_init (&intercept, master_fd, NULL);
  g_assert_true (r);
  master_fd = pty_intercept_get_fd (&intercept);
  slave_fd = pty_intercept_create_slave (master_fd, TRUE);

  pty_intercept_set_callback (&intercept, &intercept.master, on_data_recv_cb, NULL);

  launcher = g_subprocess_launcher_new (G_SUBPROCESS_FLAGS_NONE);
  g_subprocess_launcher_set_cwd (launcher, ".");
  g_subprocess_launcher_take_stdin_fd (launcher, dup (slave_fd));
  g_subprocess_launcher_take_stdout_fd (launcher, dup (slave_fd));
  g_subprocess_launcher_take_stderr_fd (launcher, dup (slave_fd));
  g_subprocess_launcher_set_child_setup (launcher, child_setup_cb, NULL, NULL);
  subprocess = g_subprocess_launcher_spawn (launcher, &error, "/bin/bash", NULL);
  g_assert_no_error (error);
  g_assert_nonnull (subprocess);

  g_subprocess_wait_async (subprocess, NULL, wait_cb, NULL);

  g_signal_connect_after (terminal, "size-allocate", G_CALLBACK (resize_window_cb), pty);
  g_signal_connect (window, "delete-event", G_CALLBACK (gtk_main_quit), NULL);
  gtk_window_present (window);

  real_intercept = &intercept;

  gtk_main ();
  pty_intercept_clear (&intercept);

  return 0;
}

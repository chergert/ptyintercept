all: ptyintercept

WARNINGS = -Wall
DEBUG = -ggdb -fno-omit-frame-pointer
OPTIMIZE = -O2
PKGS = vte-2.91

ptyintercept: Makefile ptyintercept.c test.c
	$(CC) -o $@ $(WARNINGS) $(DEBUG) $(OPTIMIZE) $(shell pkg-config --cflags --libs $(PKGS)) ptyintercept.c test.c

clean:
	rm -f ptyintercept

# Builder will call this to install the application before running.
install:
	echo "Installing is not supported"

# Builder uses this target to run your application.
run:
	./ptyintercept

